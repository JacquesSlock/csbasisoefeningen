﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace grootsteGetalWeergeven
{
    class Program
    {
        static void Main(string[] args)
        {
            int intGetal1, intGetal2, intGrootsteGetal = 0;
            Console.Write("Getal 1: ");
            intGetal1 = Convert.ToInt32(Console.ReadLine());
            Console.Write("Getal 2: ");
            intGetal2 = Convert.ToInt32(Console.ReadLine());

            if (intGetal1 > intGetal2)
            {
                intGrootsteGetal = intGetal1;
            }
            else
            {
                intGrootsteGetal = intGetal2;
            }
            Console.WriteLine("Het grootste getal is " + intGrootsteGetal);
            /*Console.WriteLine("Het grootste getal is " + Math.Max(intGetal1, intGetal2));*/

            Console.ReadLine();
        }
    }
}
