﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace kortingVoorSnelBetalen
{
    class Program
    {
        static void Main(string[] args)
        {
            double dblBedragZonderBTW, dblBedragMetBTW, dblKorting;
            int intAantalDagenNaFactuur;
            const double cdblKorting = 0.02, cdblBTW = 0.21;

            Console.Write("Aantal dagen na factuur: ");
            intAantalDagenNaFactuur = Convert.ToInt32(Console.ReadLine());

            Console.Write("Bedrag zonder BTW: ");
            dblBedragZonderBTW = Convert.ToDouble(Console.ReadLine());

            if (intAantalDagenNaFactuur <= 10)
            {
                dblKorting = dblBedragZonderBTW * cdblKorting;
                Console.WriteLine("Korting: " + dblKorting);
            }
            else
            {
                dblKorting = 0;
            }

            Console.WriteLine("Bedrag zonder korting: " + dblBedragZonderBTW);
            dblBedragZonderBTW = dblBedragZonderBTW - dblKorting;
            Console.WriteLine("Bedrag met korting: " + dblBedragZonderBTW);
            dblBedragMetBTW = dblBedragZonderBTW * cdblKorting;

            Console.WriteLine("Bedrag met BTW: " + dblBedragMetBTW);

            Console.ReadLine();
        }
    }
}
