﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bioscoopTickets
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Ontwerp een console toepassing waarmee je berekent hoeveel
            iemand voor een bioscoopkaartje moet betalen. De volle prijs van een bioscoopticket
            bedraagt 13,70 EUR.Op basis van de leeftijd wordt de prijs bepaald:
            • Jonger dan 5 jaar: gratis;
            • Tussen de 5 en de 12: halve prijs;
            • Tussen de 13 en de 54: vol tarief;
            • 55 +: gratis*/

            int intLeeftijd;
            double dblPrijs = 13.70;

            Console.Write("Leeftijd: ");
            intLeeftijd = Convert.ToInt32(Console.ReadLine());
            if (intLeeftijd < 0)
            {
                Console.WriteLine("Geen correcte leeftijd ingegeven.");
            }
            else
            {
                if (intLeeftijd < 5 || intLeeftijd >= 55)
                {
                    dblPrijs = 0;
                }
                else if (intLeeftijd >= 5 && intLeeftijd <= 12)
                {
                    dblPrijs = dblPrijs / 2.0;
                }
                else if (intLeeftijd > 12 && intLeeftijd <= 54)
                {

                }
                Console.WriteLine("De prijs is " + dblPrijs + " euro");
            }
            Console.ReadLine();
        }
    }
}
