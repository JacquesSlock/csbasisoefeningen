﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace computerEnPrinterBerekenPrijs
{
    class Program
    {
        static void Main(string[] args)
        {
            const int cintPrinter = 200;
            int intAantalComputers, intAantalPrinters;
            double dblKorting, dblTotalePrijs;
            int intPrijsPC = 1500;

            Console.WriteLine("Aantal computers: ");
            intAantalComputers = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Aantal printers: ");
            intAantalPrinters = Convert.ToInt32(Console.ReadLine());

            if (intAantalComputers > 10)
            {
                intPrijsPC = 1450;
            }

            dblTotalePrijs = intAantalComputers * intPrijsPC + intAantalPrinters * cintPrinter;

            if (dblTotalePrijs > 12500)
            {
                dblTotalePrijs *= 0.95;
            }

            Console.WriteLine("Totale prijs van " + intAantalComputers + " computers en " + intAantalPrinters
                + " printers is " + dblTotalePrijs + " euro");
            Console.ReadLine();
        }
    }
}
