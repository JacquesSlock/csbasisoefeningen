﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tweeWoordenRangschikken
{
    class Program
    {
        static void Main(string[] args)
        {
            string strWoord1, strWoord2;

            Console.WriteLine("Geef woord 1: ");
            strWoord1 = Console.ReadLine();
            Console.WriteLine("Geef woord 2: ");
            strWoord2 = Console.ReadLine();

            if (strWoord1.CompareTo(strWoord2)<0)
            {
                Console.WriteLine(strWoord1);
                Console.WriteLine(strWoord2);
            }
            else
            {
                Console.WriteLine(strWoord2);
                Console.WriteLine(strWoord1);
            }
            Console.ReadLine();
        }
    }
}
