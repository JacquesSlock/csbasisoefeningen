﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace totziensTaalkeuze
{
    class Program
    {
        static void Main(string[] args)
        {
            string strTaal;
            const string strNed = "Tot ziens", strFr = "Au revoir", strDu = "Auf wiedersehen", strEn = "Goodbye";

            Console.WriteLine("Welke taal? (Nl, Fr, Du of En");
            strTaal = Console.ReadLine();

            switch (strTaal.ToUpper()) //to upper zodat dit niet gevoelig is voor hoofdletters
            {
                case "NL":
                    Console.WriteLine(strNed);
                    break;
                case "FR":
                    Console.WriteLine(strFr);
                    break;
                case "DU":
                    Console.WriteLine(strDu);
                    break;
                case "EN":
                    Console.WriteLine(strEn);
                    break;
                default:
                    Console.WriteLine("Geef een correct antwoord aub.");
                    break;
            }
        }
    }
}
