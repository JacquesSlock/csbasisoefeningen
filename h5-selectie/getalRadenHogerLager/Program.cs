﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace getalRadenHogerLager
{
    class Program
    {
        static void Main(string[] args)
        {
            //getal aanmaken met random tussen 1 en 10
            Random rnd = new Random();
            int intComputerGetal = rnd.Next(1, 11);

            //declaratie
            int intGetal;

            //invoer
            Console.Write("Geef een getal in tussen 1 en 10: ");
            intGetal = Convert.ToInt32(Console.ReadLine());

            if (intGetal == intComputerGetal)
            {
                Console.WriteLine("Proficiat");
            }
            else if (intGetal > intComputerGetal)
            {
                Console.WriteLine("Jammer, het getal dat je geraden hebt was te hoog.");
            }
            else
            {
                Console.WriteLine("Jammer, het getal dat je geraden hebt was te laag.");
            }

            Console.ReadLine();
        }
    }
}
