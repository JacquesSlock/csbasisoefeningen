﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace rijkswachtBoetes
{
    class Program
    {
        static void Main(string[] args)
        {
            const string cstrAlQaeda = "Al Qaeda", cstrAlQaida = "Al Qaida";
            string strNaam;
            int intBoete, intMaxSnelheid, intSnelheid;
            const int cintVasteBoete = 60, cintVariableBoete = 12;



            Console.Write("Naam: ");
            strNaam = Console.ReadLine();

            if (strNaam == cstrAlQaeda || strNaam == cstrAlQaida)
            {
                Console.WriteLine("Onmiddelijk arresteren!");
            }
            else
            {
                //vragen wat max snelheid is
                Console.Write("Max snelheid: ");
                intMaxSnelheid = Convert.ToInt32(Console.ReadLine());
                //vragen hoe snel hij reed
                Console.Write("Snelheid: ");
                intSnelheid = Convert.ToInt32(Console.ReadLine());
                //kijken of hij te snel reed
                if (intSnelheid > intMaxSnelheid)
                {
                    //boete berekenen
                    intBoete = cintVasteBoete + cintVariableBoete *
                        (intSnelheid - intMaxSnelheid);
                    Console.WriteLine("De boete bedraagt " + intBoete + " euro");
                }
                else
                {
                    Console.WriteLine("Geen boete");
                }

            }
            Console.ReadLine();
            /*andere methode zonder constanten*/
            /*if (strNaam == "Al Qaeda" || strNaam == "Al Quida")
            {
                Console.WriteLine("Onmiddelijk arresteren!");
            }*/
        }
    }
}
