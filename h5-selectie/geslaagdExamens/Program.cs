﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace geslaagdExamens
{
    class Program
    {
        static void Main(string[] args)
        {
            /*            Ontwerp een consoletoepassing waarmee je aan de gebruiker het resultaat
           van 5 examens opvraagt. De opgevraagde resultaten zijn de behaalde punten op 100.De
           student is geslaagd als hij hoogstens 1 onvoldoende heeft en een gemiddelde van minstens
           50 % behaalde.*/
            double dblExamen1, dblExamen2, dblExamen3, dblExamen4, dblExamen5, dblGemiddelde;
            Boolean blnGeslaagd;
            int intAantalGeslaagdeExamens = 0;

            Console.Write("Geef je punten in voor examen 1: ");
            dblExamen1 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Geef je punten in voor examen 2: ");
            dblExamen2 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Geef je punten in voor examen 3: ");
            dblExamen3 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Geef je punten in voor examen 4: ");
            dblExamen4 = Convert.ToDouble(Console.ReadLine());

            Console.Write("Geef je punten in voor examen 5: ");
            dblExamen5 = Convert.ToDouble(Console.ReadLine());

            if (dblExamen1 >= 50)
            {
                intAantalGeslaagdeExamens++;
            }
            if (dblExamen2 >= 50)
            {
                intAantalGeslaagdeExamens++;
            }
            if (dblExamen3 >= 50)
            {
                intAantalGeslaagdeExamens++;
            }
            if (dblExamen4 >= 50)
            {
                intAantalGeslaagdeExamens++;
            }
            if (dblExamen5 >= 50)
            {
                intAantalGeslaagdeExamens++;
            }

            dblGemiddelde = (dblExamen1 + dblExamen2 + dblExamen3 + dblExamen4 + dblExamen5) / 5.0;

            if (intAantalGeslaagdeExamens >= 4 && dblGemiddelde >= 50)
            {
                blnGeslaagd = true;
            }
            else
            {
                blnGeslaagd = false;
            }

            Console.WriteLine("Je bent geslaagd: " + blnGeslaagd);
            Console.ReadLine();
        }
    }
}
