﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace voorbeeldVersusGroepskorting
{
    class Program
    {
        static void Main(string[] args)
        {
            //declaratie
            int intAantal;

            //invoer
            Console.WriteLine("Aantal personen: ");
            intAantal = Convert.ToInt32(Console.ReadLine());

            switch (intAantal)
            {
                case 1:
                case 2:
                    Console.WriteLine("Geen korting");
                    break;
                case 3:
                    Console.WriteLine("10%");
                    break;
                case 4:
                    Console.WriteLine("20%");
                    break;
                case 5:
                    Console.WriteLine("50%");
                    break;
                default:
                    Console.WriteLine("Gratis");
                    break;
            }
            Console.ReadLine();
        }
    }
}
