﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace schoenwinkelKorting
{
    class Program
    {
        static void Main(string[] args)
        {
            const double cdblKorting = 0.25;
            const int cintPrijsPerPaar = 100;
            int intAantalPaar;
            double dblTotalePrijs;

            Console.WriteLine("Aantal paar: ");
            intAantalPaar = Convert.ToInt32(Console.ReadLine());

            if (intAantalPaar >= 2)
            {
                dblTotalePrijs = intAantalPaar * (1 - cdblKorting) * cintPrijsPerPaar;
            }
            else
            {
                dblTotalePrijs = intAantalPaar * cintPrijsPerPaar;
            }

            Console.WriteLine("Totale prijs: " + dblTotalePrijs + " euro");
            Console.ReadLine();
        }
    }
}
