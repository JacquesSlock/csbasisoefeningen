﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace grootsteGetalVanDrie
{
    class Program
    {
        static void Main(string[] args)
        {
            int intG1, intG2, intG3, intGrootste;

            Console.WriteLine("G1: ");
            intG1 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("G2: ");
            intG2 = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("G3: ");
            intG3 = Convert.ToInt32(Console.ReadLine());

            if (intG1 > intG2)
            {
                if (intG1 > intG3)
                {
                    intGrootste = intG1;
                }
                else
                {
                    intGrootste = intG3;
                }
            }
            else
            {
                if (intG3 > intG2)
                {
                    intGrootste = intG3;
                }
                else
                {
                    intGrootste = intG2;
                }

            }

            Console.WriteLine("Grootste getal: " + intGrootste);
            Console.ReadLine();
        }
    }
}
