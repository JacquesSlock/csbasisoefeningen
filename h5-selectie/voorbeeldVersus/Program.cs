﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace voorbeeldVersus
{
    class Program
    {
        static void Main(string[] args)
        {
            //declaratie
            int intLeeftijd;

            //invoer
            Console.WriteLine("Leeftijd: ");
            intLeeftijd = Convert.ToInt32(Console.ReadLine());

            //uitvoer
            if (intLeeftijd >= 21)
            {
                Console.WriteLine("Je bent " + intLeeftijd + " jaar oud. "
                    + "Veel plezier in de VIP-lounge");
            }
            else if (intLeeftijd >= 16)
            {
                Console.WriteLine("Je bent " + intLeeftijd + " jaar oud. "
                    + "Veel plezier in de Versus");
            }
            else
            {
                Console.WriteLine("Helaas! Je moet minimum 16 jaar zijn.");
            }

            Console.ReadLine();
        }
    }
}
