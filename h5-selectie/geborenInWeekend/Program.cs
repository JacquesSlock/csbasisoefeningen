﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace geborenInWeekend
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime dteGeboorteDatum;

            Console.WriteLine("Wanneer ben je geboren? (dd/mm/jjjj");
            dteGeboorteDatum = Convert.ToDateTime(Console.ReadLine());

            switch (dteGeboorteDatum.DayOfWeek)
            {
                case DayOfWeek.Sunday:
                case DayOfWeek.Saturday:
                    Console.WriteLine("Je bent in het weekend geboren!");
                    break;
                case DayOfWeek.Monday:                
                case DayOfWeek.Tuesday:
                case DayOfWeek.Wednesday:
                case DayOfWeek.Thursday:
                case DayOfWeek.Friday:
                    Console.WriteLine("Je bent niet in het weekend geboren.");
                    break;

                default:
                    break;
            }


        }
    }
}
