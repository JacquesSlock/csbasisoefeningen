﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace schoenwinkelExtraKorting
{
    class Program
    {
        static void Main(string[] args)
        {
            double dblKorting = 0;
            const int cintPrijsPerPaar = 125;
            int intAantalPaar;
            double dblTotalePrijs;

            Console.WriteLine("Aantal paar: ");
            intAantalPaar = Convert.ToInt32(Console.ReadLine());

            switch (intAantalPaar)
            {
                case 0:
                    dblKorting = 0;
                    break;
                case 1:
                    dblKorting = 0.2;
                    break;
                case 2:
                    dblKorting = 0.4;
                    break;
                /* case 3:
                    dblKorting = 0.6;
                    break;*/
                default:
                    dblKorting = 0.6;
                    break;
            }

            dblTotalePrijs = cintPrijsPerPaar * intAantalPaar * (1.0 - dblKorting);

            Console.WriteLine("De totale prijs is " + dblTotalePrijs);
            Console.ReadLine();
        }
    }
}
