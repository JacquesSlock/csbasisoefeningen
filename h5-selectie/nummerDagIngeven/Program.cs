﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nummerDagIngeven
{
    class Program
    {
        static void Main(string[] args)
        {
            int intDag;
            string strDag = "";
            Console.Write("Dag van de week: ");
            intDag = Convert.ToInt32(Console.ReadLine());

            switch (intDag)
            {
                case 1:
                    strDag = "Maandag";
                    break;
                case 2:
                    strDag = "Dinsdag";
                    break;
                case 3:
                    strDag = "Woensdag";
                    break;
                case 4:
                    strDag = "Donderdag";
                    break;
                case 5:
                    strDag = "Vrijdag";
                    break;
                case 6:
                    strDag = "Zaterdag";
                    break;
                case 7:
                    strDag = "Zondag";
                    break;
                default:
                    strDag = "Geen geldige waarde";
                    break;
            }
            Console.WriteLine(strDag);
            Console.ReadLine();
        }
    }
}
