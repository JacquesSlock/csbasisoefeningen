﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace voorbeeldVersusLadiesNight
{
    class Program
    {
        static void Main(string[] args)
        {
            //declaratie
            int intLeeftijd;
            Boolean blnGehuwd, blnGeslacht;

            //invoer
            Console.Write("Leeftijd: ");
            intLeeftijd = Convert.ToInt32(Console.ReadLine());

            Console.Write("Ben je ongehuwd? True of false");
            blnGehuwd = Convert.ToBoolean(Console.ReadLine());

            Console.Write("Wat is je geslacht? M of V.");
            string strGeslacht = Console.ReadLine().ToUpper();
            /*if(strGeslacht=="V")
            {
                blnGeslacht = true;
            }
            else
            {
                blnGeslacht = false;
            }*/
            if (strGeslacht == "V") blnGeslacht = true; else blnGeslacht = false; // dit doet hetzelfde als de andere if else, maar korter en minder duidelijk

            //uitvoer
            if (intLeeftijd >= 30 && blnGehuwd && blnGeslacht)
            {
                Console.WriteLine("Je krijgt toegang tot de ladies night");
            }
            else
            {
                Console.WriteLine("Helaas! Geen toegang vanavond.");
            }

            Console.ReadLine();
        }
    }
}
