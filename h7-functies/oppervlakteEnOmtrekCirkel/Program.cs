﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oppervlakteEnOmtrekCirkel
{
    class Program
    {
        static void Main(string[] args)
        {
            static void Main(string[] args)
            {
                double dblStraal;

                Console.WriteLine("Geef een straal in");
                dblStraal = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Oppervlakte: " + oppervlakteCirkel(dblStraal));
                Console.WriteLine("Omtrek: " + omtrekCirkel(dblStraal));
                Console.ReadLine();
            }

            static double oppervlakteCirkel(double pdblStraal)
            {
                double dblOppervlakte;
                dblOppervlakte = Math.PI * Math.Pow(pdblStraal, 2);
                return dblOppervlakte;
            }
            static double omtrekCirkel(double pdblStraal)
            {
                double dblOmtrek;
                dblOmtrek = Math.PI * pdblStraal * 2;
                return dblOmtrek;
            }
        }
    }
}
