﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace naamFamilienaamMetHoofdletter
{
    class Program
    {

        static void Main(string[] args)
        {
            string strVoornaam, strFamilienaam;
            Console.WriteLine("Geef uw voornaam: ");
            strVoornaam = Console.ReadLine();
            Console.WriteLine("Geef uw familienaam: ");
            strFamilienaam = Console.ReadLine();
            drukNaamAf(strVoornaam, strFamilienaam);

            Console.WriteLine("Je volledige naam is: " + drukNaamAfViaString(strVoornaam, strFamilienaam));

            Console.ReadLine();
        }

        static void drukNaamAf(string pstrVoornaam, string pstrFamilienaam)
        {
            string strOutput;
            strOutput = pstrVoornaam.Substring(0, 1).ToUpper() + pstrVoornaam.Substring(1) +
                " " + pstrFamilienaam.Substring(0, 1).ToUpper() + pstrFamilienaam.Substring(1);
            Console.WriteLine("Je volledige naam is: " + strOutput);
        }

        static string drukNaamAfViaString(string pstrVoornaam, string pstrFamilienaam)
        {
            string strOutput;
            strOutput = pstrVoornaam.Substring(0, 1).ToUpper() + pstrVoornaam.Substring(1) +
                " " + pstrFamilienaam.Substring(0, 1).ToUpper() + pstrFamilienaam.Substring(1);
            return strOutput;
        }
    }
}

