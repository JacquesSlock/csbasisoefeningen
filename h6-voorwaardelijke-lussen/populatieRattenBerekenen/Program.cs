﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace populatieRattenBerekenen
{
    class Program
    {
        static void Main(string[] args)
        {
            int intAantalRatten = 2000000, intAantalJaar = 0;
            const double cdblGroei = 0.15;

            while (intAantalRatten < 10000000)
            {
                intAantalRatten = Convert.ToInt32(intAantalRatten * (1 + cdblGroei));
                Console.WriteLine("Jaar " + intAantalJaar + ": " +
                    intAantalRatten.ToString("0 000 000") + " ratten.");
                intAantalJaar = intAantalJaar + 1;
                //of intAantal++
            }
            Console.WriteLine("Na " + intAantalJaar + " jaar bedraagt de populatie ratten " +
                intAantalRatten.ToString("0 000 000"));
            Console.ReadLine();
        }
    }
}
