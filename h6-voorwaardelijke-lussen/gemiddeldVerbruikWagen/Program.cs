﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gemiddeldVerbruikWagen
{
    class Program
    {
        static void Main(string[] args)
        {
            double dblAantalLiter,
               dblAantalKilometer,
               dblTotaalLiter = 0,
               dblTotaalKilometer = 0,
               dblGemiddelde;

            do
            {
                Console.Write("L:");
                dblAantalLiter = Convert.ToDouble(Console.ReadLine());
                dblTotaalLiter += dblAantalLiter;
                if (dblAantalLiter != 0)
                {
                    Console.Write("KM:");
                    dblAantalKilometer = Convert.ToDouble(Console.ReadLine());
                    dblTotaalKilometer += dblAantalKilometer;
                }

            } while (dblAantalLiter != 0);


            dblGemiddelde = (dblTotaalLiter / dblTotaalKilometer) * 100;
            Console.WriteLine("Het gemiddelde verbruik per 100 km is:  " + dblGemiddelde + " L");
            Console.ReadLine();
        }
    }
}
