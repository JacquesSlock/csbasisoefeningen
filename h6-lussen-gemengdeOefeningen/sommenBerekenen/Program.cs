﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sommenBerekenen
{
    class Program
    {
        static void Main(string[] args)
        {
            double dblSom1 = 0, dblSom2 = 0, dblSom3 = 0;
            for (int i = 1; i <= 10; i++)
            {
                dblSom1 += 1.0 / i;
            }

            Console.WriteLine(Math.Round(dblSom1,1));

            for (int i = 1; i <= 10; i++)
            {
                if (i%2==0)
                {
                    dblSom2 -= i;
                }
                else
                {
                    dblSom2 += i;
                }
                
            }

            Console.WriteLine(dblSom2);

            for (int i = 1; i <= 10; i++)
            {
                if (i % 2 == 0)
                {
                    dblSom3 -= 1.0 /i;
                }
                else
                {
                    dblSom3 += 1.0 /i;
                }
            }

            Console.WriteLine(Math.Round(dblSom3, 1));
            Console.ReadLine();
        }
    }
}
