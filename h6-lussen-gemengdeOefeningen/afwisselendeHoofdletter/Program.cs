﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace afwisselendeHoofdletter
{
    class Program
    {
        static void Main(string[] args)
        {
            string strWoord, strUitvoer = "";

            Console.WriteLine("Geef een woord in: ");
            strWoord = Console.ReadLine();

            for (int i = 0; i < strWoord.Length; i++)
            {
                if (i%2==0)
                {
                    strUitvoer += strWoord.Substring(i, 1).ToUpper();
                }
                else
                {
                    strUitvoer += strWoord.Substring(i, 1).ToLower(); //tolower is op zich niet nodig
                }
                
            }
            Console.WriteLine();
            Console.WriteLine(strUitvoer);
            Console.ReadLine();
        }
    }
}
