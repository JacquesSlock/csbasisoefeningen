﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace theorieOef2
{
    class Program
    {
        static void Main(string[] args)
        {
            int intTeller = 0;
            int intResultaat = 2;
            do
            {
                intTeller += 1;
                intResultaat += (intTeller * 3);
            }
            while ((intResultaat % 2) == 0);
            Console.WriteLine(intResultaat + " - " +intTeller);
            Console.ReadLine();
        }
    }
}
