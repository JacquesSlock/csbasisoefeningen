﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lettersVanWoordOnderElkaar
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Laat de gebruiker een woord ingeven. 
            * Toon iedere letter onder elkaar in het consolescherm.
           */
            string strWoord;

            Console.WriteLine("Geef een woord in: ");
            strWoord = Console.ReadLine();

            for (int i = 0; i < strWoord.Length; i++)
            {
                Console.WriteLine(strWoord.Substring(i, 1));
            }

            Console.ReadLine();
        }
    }
}
