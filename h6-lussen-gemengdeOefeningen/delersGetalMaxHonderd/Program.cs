﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace delersGetalMaxHonderd
{
    class Program
    {
        static void Main(string[] args)
        {
            int intGetal;
            Console.WriteLine("Geef een getal in: (max. 100)");
            
            // blijf een getal vragen tot het kleiner is dan 100
            do
            {
                intGetal = Convert.ToInt32(Console.ReadLine());
            } while (intGetal > 100);

            for (int i = 1; i <= intGetal; i++)
            {
                int intAantalDelers = 0;
                Console.WriteLine("Getal " + i);
                Console.Write("Delers: ");
                for (int intDeler = 1; intDeler <= i; intDeler++)
                {
                    if (i % intDeler == 0)
                    {
                        Console.Write(intDeler + " ");
                        intAantalDelers++;
                    }
                }
                Console.WriteLine();
                if (intAantalDelers==2)
                {
                    Console.Write("priemgetal");
                    Console.WriteLine();
                }
                Console.WriteLine();
            }
            Console.ReadLine();

        }
    }
}
