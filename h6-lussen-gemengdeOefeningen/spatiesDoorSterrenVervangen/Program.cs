﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace spatiesDoorSterrenVervangen
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Schrijf een programma om de spaties uit een ingevoerde zin om te zetten in
            sterretjes.
            (je kan dit natuurlijk ook oplossen door de functie Replace te gebruiken, maar probeer beide
            mogelijkheden!)
            */

            string strZin, strNieuwezin = "";
            Console.WriteLine("Geef een zin in: ");
            strZin = Console.ReadLine();

            /*for (int i = 0; i < strZin.Length; i++)
            {
                if(strZin.Substring(i,1) == " ")
                {
                    strNieuwezin += "*";
                }
                else
                {
                    strNieuwezin += strZin.Substring(i, 1);
                }
            }*/
            strNieuwezin = strZin.Replace(" ", "*");

            Console.WriteLine(strNieuwezin);
            Console.ReadLine();
        }
    }
}
