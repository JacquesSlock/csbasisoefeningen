﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sterretjesDriehoek
{
    class Program
    {
        static void Main(string[] args)
        {
            int intGetal;
            
            Console.Write("Getal: ");
            intGetal = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();

            // oplossing 2 - iets meer functies die opgeroepen worden
            for (int i = 0; i < intGetal; i++)
            {
                string strRij = "";
                for (int j = 0; j < i; j++)
                {
                    // Console.Write("*");
                    strRij += "*";
                }
                Console.WriteLine(strRij);
            }



            Console.ReadLine();
        }
    }
}
