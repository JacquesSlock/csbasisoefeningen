﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace optelSommen
{
    class Program
    {
        static void Main(string[] args)
        {
            /* Ontwerp een consoletoepassing waarin de gebruiker 5 optelsommen te
            zien krijgt. Wanneer de gebruiker een fout antwoord geeft dan weerklinkt een beep.Op het
            einde van het programma verschijnen de resultaten in de console.
            Om willekeurige getallen te genereren gebruiken we de randomgenerator “Random random
            = new Random()”.*/

            int intAantalCorrect = 0;

            for (int i = 0; i < 5; i++)
            {
                int intGetal1, intGetal2, intSom, intOplossing;
                Random rnd = new Random();
                intGetal1 = rnd.Next(1, 11);
                intGetal2 = rnd.Next(1, 11);
                intSom = intGetal1 + intGetal2;
                Console.Write("De som van " + intGetal1 + " + " + intGetal2 + " = ");
                intOplossing = Convert.ToInt32(Console.ReadLine());
                if (intSom == intOplossing)
                {
                    Console.WriteLine("Correct");
                    intAantalCorrect++;
                }
                else
                {
                    Console.Beep();
                }
            }

            Console.WriteLine("Je had " + intAantalCorrect + " sommen correct");
            Console.ReadLine();
        }
    }
}
