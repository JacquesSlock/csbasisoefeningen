﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sterretjesVierkant
{
    class Program
    {
        static void Main(string[] args)
        {
            int intGetal;
            string strRij = "";
            Console.Write("Getal: ");
            intGetal = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine();

            // oplossing 1
            for (int i = 0; i < intGetal; i++)
            {
                strRij += "*";
            }

            for (int i = 0; i < intGetal; i++)
            {
                Console.WriteLine(strRij);
            }
            Console.WriteLine();

            // oplossing 2 - iets meer functies die opgeroepen worden
            for (int i = 0; i < intGetal; i++)
            {
                for (int j = 0; j < intGetal; j++)
                {
                    Console.Write("*");
                }
                Console.WriteLine();
            }



            Console.ReadLine();
        }
    }
}
