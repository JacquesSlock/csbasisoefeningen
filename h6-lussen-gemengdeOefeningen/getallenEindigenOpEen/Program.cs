﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace getallenEindigenOpEen
{
    class Program
    {
        static void Main(string[] args)
        {
            int intGetallenDieEindigenOpEen = 0;
            int intGetal;

            for (int i = 1; i <= 10; i++)
            {
                Console.Write("Getal " + i);
                intGetal = Convert.ToInt32(Console.ReadLine());
                if (intGetal % 10 == 1)
                {
                    intGetallenDieEindigenOpEen++;
                }
            }

            Console.WriteLine("Er waren " + intGetallenDieEindigenOpEen +
                "getallen die eindigen op 1");

            Console.ReadLine();
        }
    }
}
