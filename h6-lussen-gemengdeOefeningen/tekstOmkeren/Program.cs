﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tekstOmkeren
{
    class Program
    {
        static void Main(string[] args)
        {
            /*Schrijf een programma waarbij een ingegeven tekst omgekeerd wordt.
            Bijvoorbeeld:
            Invoer: oefeningen
            Uitvoer: negninefeo*/

            string strInvoer, strUitvoer = "";
            Console.WriteLine("Geef hier je tekst in: ");
            strInvoer = Console.ReadLine();

            for (int i = strInvoer.Length - 1; i >= 0; i--)
            {
                strUitvoer += strInvoer.Substring(i, 1);
            }

            Console.WriteLine(strUitvoer);
            Console.ReadLine();
        }
    }
}
