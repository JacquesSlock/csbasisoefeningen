﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dobbelenMetInzet
{
    class Program
    {
        static void Main(string[] args)
        {
            int intWorp1, intWorp2, intInzet, intAantalKeerTwaalf = 0, intAantalKeerHetzelfde = 0, intAantalKeerZes = 0, intWinst;
            Random rnd = new Random();
            do
            {
                Console.Write("Inzet tussen 5 en 100: ");
                intInzet = Convert.ToInt32(Console.ReadLine());
            } while (intInzet < 5 || intInzet > 100);


            for (int i = 0; i < 3; i++)
            {
                intWorp1 = rnd.Next(1, 7);
                intWorp2 = rnd.Next(1, 7);

                if (intWorp1 == 6 && intWorp2 == 6)
                {
                    intAantalKeerTwaalf++;
                }
                else if (intWorp1 == intWorp2)
                {
                    intAantalKeerHetzelfde++;
                }
                else if (intWorp1 == 6 || intWorp2 == 6)
                {
                    intAantalKeerZes++;
                }
            }
            if (intAantalKeerTwaalf > 0)
            {
                intWinst = intInzet * 50;
            }
            else if (intAantalKeerHetzelfde > 0)
            {
                intWinst = intInzet * 10;
            }
            else if (intAantalKeerZes > 1)
            {
                intWinst = intInzet * 2;
            }
            else
            {
                intWinst = 0;
            }

            Console.WriteLine("Winst is " + intWinst);
            Console.ReadLine();
        }
    }
}
