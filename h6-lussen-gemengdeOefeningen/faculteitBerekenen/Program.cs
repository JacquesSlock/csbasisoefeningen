﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace faculteitBerekenen
{
    class Program
    {
        static void Main(string[] args)
        {
            int intGetal, intResultaat = 1;

            Console.Write("Geef een getal in aub: ");
            intGetal = Convert.ToInt32(Console.ReadLine());

            for (int i = 1; i <= intGetal; i++) // dit kan ook met een omgekeerde for lus
            {
                intResultaat = intResultaat * i;
            }
            Console.WriteLine(intResultaat);
            Console.ReadLine();
        }
    }
}
