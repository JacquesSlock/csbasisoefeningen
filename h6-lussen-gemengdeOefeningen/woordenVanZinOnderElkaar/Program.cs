﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace woordenVanZinOnderElkaar
{
    class Program
    {
        static void Main(string[] args)
        {
            string strZin, strNieuwezin = "";
            Console.WriteLine("Geef een zin in: ");
            strZin = Console.ReadLine();
            // oplossing 1
            /*for (int i = 0; i < strZin.Length; i++)
            {
                if(strZin.Substring(i,1) == " ")
                {
                    strNieuwezin += Environment.NewLine;
                }
                else
                {
                    strNieuwezin += strZin.Substring(i, 1);
                }
            }*/

            // oplossing 2
            for (int i = 0; i < strZin.Length; i++)
            {
                if (strZin.Substring(i, 1) == " ")
                {
                    Console.WriteLine();
                }
                else
                {
                    Console.Write(strZin.Substring(i, 1));
                }
            }
            Console.WriteLine();

            // oplossing 3
/*            strNieuwezin = strZin.Replace(" ", Environment.NewLine);*/

            Console.WriteLine(strNieuwezin);
            Console.ReadLine();
        }
    }
}
