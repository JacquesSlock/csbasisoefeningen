﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace dagenTotVerjaardag
{
    class Program
    {
        static void Main(string[] args)
        {
            //declaratie
            DateTime dteVerjaardag, dteNu = DateTime.Today;
            int intDagenTotVerjaardag;

            //invoer
            Console.WriteLine("Wat is je verjaardag? (dd/mm)");
            dteVerjaardag = Convert.ToDateTime(Console.ReadLine());
            intDagenTotVerjaardag = Math.Abs((dteVerjaardag - dteNu).Days);

            //verwerking
            Console.WriteLine("Het aantal dagen tot je verjaardag is " + intDagenTotVerjaardag);

            Console.ReadLine();

        }
    }
}
