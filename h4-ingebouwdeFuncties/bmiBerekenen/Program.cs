﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bmiBerekenen
{
    class Program
    {
        static void Main(string[] args)
        {
            //variabelen
            double dblGewicht, dblLengte, dblBMI;


            //invoer
            Console.Write("Gewicht: ");
            dblGewicht = Convert.ToDouble(Console.ReadLine());

            Console.Write("Lengte (in cm): ");
            dblLengte = Convert.ToDouble(Console.ReadLine());


            //verwerking
            dblLengte = dblLengte / 100; //omzetten naar meter

            dblBMI = dblGewicht / Math.Pow(dblLengte, 2);
            dblBMI = Math.Round(dblBMI);

            //uitvoer
            Console.WriteLine("BMI: " + dblBMI);
            Console.ReadLine();
        }
    }
}
