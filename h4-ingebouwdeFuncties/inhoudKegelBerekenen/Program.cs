﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace inhoudKegelBerekenen
{
    class Program
    {
        static void Main(string[] args)
        {
            // declaratie
            double dblInhoud, dblStraal, dblHoogte;

            // invoer
            Console.Write("Straal: ");
            dblStraal = Convert.ToDouble(Console.ReadLine());
            Console.Write("Hoogte: ");
            dblHoogte = Convert.ToDouble(Console.ReadLine());

            // verwerking
            dblInhoud = Math.PI * Math.Pow(dblStraal, 2) * dblHoogte / 3;

            dblInhoud = Math.Round(dblInhoud, 2);


            // uitvoer
            Console.WriteLine("Inhoud: " + dblInhoud);
            Console.ReadLine();
        }
    }
}
