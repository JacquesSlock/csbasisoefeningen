﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace eindkapitaalInterestBerekenen
{
    class Program
    {
        static void Main(string[] args)
        {
            //variabelen
            double dblEindKapitaal, dblBeginKapitaal, dblInterest;
            int intAantalJaar;

            //invoer
            Console.Write("Beginkapitaal: ");
            dblBeginKapitaal = Convert.ToDouble(Console.ReadLine());
            Console.Write("Interest: ");
            dblInterest = Convert.ToDouble(Console.ReadLine());
            Console.Write("Aantal jaren: ");
            intAantalJaar = Convert.ToInt32(Console.ReadLine());

            //verwerking
            dblEindKapitaal = dblBeginKapitaal *
                Math.Pow((1 + (dblInterest / 100)), intAantalJaar);

            //uitvoer
            Console.WriteLine("Eindkapitaal: " + dblEindKapitaal);
            Console.ReadLine();
        }
    }
}
