﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aantalDagenInLeven
{
    class Program
    {
        static void Main(string[] args)
        {
            //variabelen
            DateTime dteGeboorteDatum, dteNu;
            int intAantalDagen, intJaar;
            dteNu = DateTime.Today;


            //invoer
            Console.Write("Wat is je geboortedatum? ");
            dteGeboorteDatum = Convert.ToDateTime(Console.ReadLine());

            /*intAantalDagen = (dteNu - dteGeboorteDatum).Days;*/
            intAantalDagen = dteNu.Subtract(dteGeboorteDatum).Days;
            intJaar = dteNu.Year - dteGeboorteDatum.Year;

            //uitvoer
            Console.WriteLine("Jij bent geboren op " +
                dteGeboorteDatum.ToLongDateString());
            Console.WriteLine("Jij leeft al " + intAantalDagen + " dagen!");
            Console.WriteLine("Jij bent " + intJaar + " jaar.");
            Console.WriteLine("De dag van de maand voor vandaag is " +
                dteNu.Day);

            Console.ReadLine();
        }
    }
}
