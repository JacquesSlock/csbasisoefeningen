﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace substringCursusProgrammeren
{
    class Program
    {
        static void Main(string[] args)
        {
            string strTekst = "Cursus programmeren in C#";
            Console.WriteLine(strTekst.Substring(0, 2));
            Console.WriteLine(strTekst.Substring(7, 7));
            Console.WriteLine(strTekst.Substring(strTekst.Length - 1, 1));

            string strIMG = "image.jpg";
            Console.WriteLine("De extentie is " + strIMG.Substring(strIMG.Length - 4, 4));
            Console.ReadLine();
        }
    }
    }
}
