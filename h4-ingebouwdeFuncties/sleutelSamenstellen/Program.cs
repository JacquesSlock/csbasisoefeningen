﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sleutelSamenstellen
{
    class Program
    {
        static void Main(string[] args)
        {
            //variabelen
            string strVoornaam, strFamilieNaam, strSleutel;
            Random rndGetal = new Random();
            DateTime dteGeboorte;

            //invoer
            Console.WriteLine("Voornaam?");
            strVoornaam = Console.ReadLine();
            Console.WriteLine("Familienaam?");
            strFamilieNaam = Console.ReadLine();
            Console.WriteLine("Geboortedatum? - dd/mm/YYYY");
            dteGeboorte = Convert.ToDateTime(Console.ReadLine());

            //verwerking
            strSleutel = strFamilieNaam.Substring(0, 2).ToUpper();
            strSleutel += rndGetal.Next(0, 11);
            strSleutel += strVoornaam.Substring(strVoornaam.Length - 2, 2).ToLower();
            /*            strSleutel += dteGeboorte.Day;
                        strSleutel += dteGeboorte.Month;
                        strSleutel += dteGeboorte.Year;*/
            strSleutel += dteGeboorte.Day + "" + dteGeboorte.Month + "" + dteGeboorte.Year;

            //uitvoer
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("jouw sleutel is:".ToUpper() + strSleutel);
            Console.ReadLine();
        }
    }
}
