﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace oppervlakteOmtrekCirkel
{
    class Program
    {
        static void Main(string[] args)
        {
            //variabelen
            double dblStraal, dblOmtrek, dblOpp;

            //invoer
            Console.WriteLine("Geef de straal in aub");
            dblStraal = Convert.ToDouble(Console.ReadLine());

            //verwerking
            dblOpp = Math.Pow(dblStraal, 2) * Math.PI;
            dblOmtrek = 2 * dblStraal * Math.PI;

            //afronden
            dblOpp = Math.Round(dblOpp, 1);
            dblOmtrek = Math.Round(dblOmtrek, 1);

            //uitvoer
            Console.WriteLine("Een cirkel met straal " +
                dblStraal + " heeft een oppervlakte van " + dblOpp
                + " en een omtrek van " + dblOmtrek);
            Console.ReadLine();

        }
    }
}
