﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tandartsBezoekDatum
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime dteVorigBezoek, dteVolgendBezoek;
            int intAantalDagenTotVolgendeBezoek;

            Console.WriteLine("Wanneer was je vorig bezoek? (dd/mm/jjj)");
            dteVorigBezoek = Convert.ToDateTime(Console.ReadLine());
            dteVolgendBezoek = dteVorigBezoek.AddMonths(6);
            intAantalDagenTotVolgendeBezoek = (dteVolgendBezoek - dteVorigBezoek).Days;

            Console.WriteLine("Je volgend bezoek is op " + dteVolgendBezoek.ToShortDateString());
            Console.WriteLine("Er zijn nog " + intAantalDagenTotVolgendeBezoek + " dagen tot het volgende bezoek.");
            Console.ReadLine(); 
        }
    }
}
