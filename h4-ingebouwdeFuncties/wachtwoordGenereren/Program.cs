﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wachtwoordGenereren
{
    class Program
    {
        static void Main(string[] args)
        {
            //variabelen
            string strVoornaam, strFamilieNaam, strSleutel;
            Random rndGetal = new Random();
            int intTelefoonNummer, intPostcode;

            //invoer
            Console.Write("Voornaam: ");
            strVoornaam = Console.ReadLine();
            Console.Write("Familienaam: ");
            strFamilieNaam = Console.ReadLine();
            Console.Write("Telefoonnummer: ");
            intTelefoonNummer = Convert.ToInt32(Console.ReadLine());
            Console.Write("Postcode: ");
            intPostcode = Convert.ToInt32(Console.ReadLine());

            //verwerking
            strSleutel = strFamilieNaam.Substring(1, 1).ToLower() + strFamilieNaam.Substring(0, 1).ToUpper();
            strSleutel += intTelefoonNummer.ToString().Substring(0,1);
            strSleutel += Math.Pow(intPostcode, 2).ToString().Substring(0, 1);

            //uitvoer
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("jouw wachtwoord is:".ToUpper() + strSleutel);
            Console.ReadLine();
        }
    }
}
