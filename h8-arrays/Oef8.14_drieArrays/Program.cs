﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef8._14_drieArrays
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] dblArr1 = new double[5];
            double[] dblArr2 = new double[5];
            double[] dblArr3 = new double[5];
            Random rnd = new Random();
            string strOut;

            for (int i = 0; i < dblArr1.Length; i++)
            {
                dblArr1[i] = rnd.Next(1, 11);
                dblArr2[i] = rnd.Next(1, 11);
                dblArr3[i] = dblArr1[i] + dblArr2[i];
            }

            for (int i = 0; i < dblArr1.Length; i++)
            {
                strOut = dblArr1[i] + " + " + dblArr2[i] + " = " + dblArr3[i];
                Console.WriteLine(strOut);
            }
           
            Console.ReadLine();

        }
    }
}
