﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef8._4
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arrGetallen = new int[7];
            int intTeTonen;
            for (int i = 0; i < 7; i++)
            {
                Console.WriteLine("Getal " + (i+1));
                arrGetallen[i] = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine("Welk getal wil je terug zien?");
            intTeTonen = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("Getal met index " + intTeTonen + " is " + arrGetallen[intTeTonen]);

            Console.ReadLine();
        }
    }
}
