﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef8._9
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] arrWoorden = new string[10];
            int intAantalKortsteWoorden = 0, intAantalLangsteWoorden = 0, intLengteKortsteWoord = 10, intLengteLangsteWoord = 0; ;
            string strKortsteWoorden = "", strLangsteWoorden = "", strOutput = "";

            for (int i = 0; i < arrWoorden.Length; i++)
            {
                Console.WriteLine("Woord:");
                arrWoorden[i] = Console.ReadLine();
            }

            for (int i = 0; i < arrWoorden.Length; i++)
            {
                if (arrWoorden[i].Length > strLangsteWoorden.Split(',')[0].Length)
                {
                    strLangsteWoorden = arrWoorden[i];
                    intLengteLangsteWoord = arrWoorden[i].Length;
                    intAantalLangsteWoorden = 1;
                }
                else if (arrWoorden[i].Length == strLangsteWoorden.Split(',')[0].Length)
                {
                    strLangsteWoorden += ", " + arrWoorden[i];
                    intAantalLangsteWoorden++;
                }

                if (arrWoorden[i].Length < intLengteKortsteWoord)
                {
                    strKortsteWoorden = arrWoorden[i];
                    intLengteKortsteWoord = arrWoorden[i].Length;
                    intAantalKortsteWoorden = 1;
                }
                else if (arrWoorden[i].Length == intLengteKortsteWoord)
                {
                    strKortsteWoorden += ", " + arrWoorden[i];
                    intAantalKortsteWoorden++;
                }

            }

            if (intAantalKortsteWoorden > 1)
            {
                strOutput = "Er zijn " + intAantalKortsteWoorden + " woorden het kortst: " + strKortsteWoorden + System.Environment.NewLine;
            }
            else
            {
                strOutput = "Het kortste woord is: " + strKortsteWoorden + System.Environment.NewLine;

            }

            if (intAantalLangsteWoorden > 1)
            {
                strOutput += "Er zijn " + intAantalLangsteWoorden + " het langst: " + strLangsteWoorden;
            }
            else
            {
                strOutput += "Het langste woord is: " + strLangsteWoorden;

            }

            Console.WriteLine(strOutput);
            Console.ReadLine();
        }
    }
}
