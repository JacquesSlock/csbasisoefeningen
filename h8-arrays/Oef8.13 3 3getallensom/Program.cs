﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef8._13_3_3getallensom
{
    class Program
    {
        static void Main(string[] args)
        {
            double[] dblArr = new double[3];
            double dblSom = 0;
            for (int i = 0; i < dblArr.Length; i++)
            {
                Console.Write("Getal: ");
                dblArr[i] = Convert.ToInt32(Console.ReadLine());
            }

            for (int i = 0; i < dblArr.Length; i++)
            {
                dblSom += dblArr[i];
            }

            Console.WriteLine("Uitkomst: " + dblSom);
            Console.ReadLine();
        }
    }
}
