﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef8._10
{
    class Program
    {
        static void Main(string[] args)
        {
            int[,] intM = new int[2, 3];

            intM[0, 0] = 5;
            intM[1, 1] = 3;
            intM[1, 2] = 7;

            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Console.Write(intM[i,j] + " ");
                }
                Console.WriteLine();
            }
            Console.ReadLine();
        }
    }
}
