﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef8._16_arrayNamenAlfabetisch
{
    class Program
    {
        static void Main(string[] args)
        {
            int intAantalNamen;

            Console.WriteLine("Hoeveel namen wil je ingeven? ");
            intAantalNamen = Convert.ToInt32(Console.ReadLine());

            string[] arrNamen = new string[intAantalNamen];
            for (int i = 0; i < intAantalNamen; i++)
            {
                Console.WriteLine("Naam " + (i + 1));
                arrNamen[i] = Console.ReadLine();
            }

            Array.Sort(arrNamen);
            for (int i = 0; i < intAantalNamen; i++)
            {
                Console.Write(arrNamen[i] + ", ");
            }
            Console.ReadLine();
           
        }
    }
}
