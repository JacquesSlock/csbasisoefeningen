﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef8._8
{
    class Program
    {
        static void Main(string[] args)
        {
            int intAantalWorpen;
            int[] arrGegooid = new int[11];

            Random rnd = new Random();

            
            Console.Write("Hoeveel keer wil je werpen? ");
            intAantalWorpen = Convert.ToInt32(Console.ReadLine());

            for (int i = 0; i < intAantalWorpen; i++)
            {
                int intWorp = rnd.Next(1, 7) + rnd.Next(1, 7);
                arrGegooid[intWorp - 2]++;
            }

            Console.WriteLine();

            for (int i = 0; i < arrGegooid.Length; i++)
            {
                Console.Write((i+2) +  " ogen: ");
                Console.WriteLine(arrGegooid[i]);
            }
            Console.ReadLine();
        }
    }
}
