﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef8._7
{
    class Program
    {
        static void Main(string[] args)
        {
            int intAantalGetallen, intNaTeKijken;

            Console.WriteLine("Hoeveel getallen wil je ingeven? ");
            intAantalGetallen = Convert.ToInt32(Console.ReadLine());

            int[] arrGetallen = new int[intAantalGetallen];
            for (int i = 0; i < intAantalGetallen; i++)
            {
                Console.WriteLine("Getal " + (i + 1));
                arrGetallen[i] = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine();
            Console.WriteLine("Na te kijken getal");
            intNaTeKijken = Convert.ToInt32(Console.ReadLine());

            for (int i = 0; i < intAantalGetallen; i++)
            {
                if (arrGetallen[i]== intNaTeKijken)
                {
                    Console.WriteLine("Het getal is opgenomen in de rij.");
                    break;
                }
            }
            Console.ReadLine();
        }
    }
}
