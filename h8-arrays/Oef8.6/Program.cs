﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Oef8._6
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arrGetallen = new int[5];
            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine("Getal " + (i + 1));
                arrGetallen[i] = Convert.ToInt32(Console.ReadLine());
            }

            Console.WriteLine();
            Console.WriteLine("Even getallen");

            for (int i = 0; i < 10; i++)
            {
                if (arrGetallen[i]%2==0)
                {
                    Console.WriteLine(", " + arrGetallen[i]);
                }
            }
            Console.ReadLine();
        }
    }
}
