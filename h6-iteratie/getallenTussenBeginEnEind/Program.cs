﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace getallenTussenBeginEnEind
{
    class Program
    {
        static void Main(string[] args)
        {
            int intBegin, intEind, intStapgrootte;
            Console.Write("Beginwaarde: ");
            intBegin = Convert.ToInt32(Console.ReadLine());
            Console.Write("Eindwaarde: ");
            intEind = Convert.ToInt32(Console.ReadLine());
            Console.Write("Stapgrootte: ");
            intStapgrootte = Convert.ToInt32(Console.ReadLine());

            for (int i = intBegin; i <= intEind; i+= intStapgrootte) // LET OP: <=intEind
            {
                Console.WriteLine(i);
            }

            Console.ReadLine();
        }
    }
}
