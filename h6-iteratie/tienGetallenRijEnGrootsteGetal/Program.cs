﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tienGetallenRijEnGrootsteGetal
{
    class Program
    {
        static void Main(string[] args)
        {
            //zoek het grootste van 10 getallen: oefening 6-7
            //variabelen declareren
            int intTeller, intGetal, intGrootste = 0;
            string strRij = "";

            //vraag 10 keer naar een getal:
            for (intTeller = 1; intTeller <= 10; intTeller++)
            {
                Console.Write("Geef getal " + intTeller + ": ");
                intGetal = Convert.ToInt16(Console.ReadLine());

                //voeg dit getal toe aan de rij:
                strRij += intGetal + " ";

                //zoek het grootste getal:
                if (intGetal > intGrootste)
                {
                    intGrootste = intGetal;
                }
            }
            //uitvoer:
            Console.WriteLine("De ingegeven getallen zijn: " + strRij);
            Console.WriteLine(intGrootste + " is het grootste getal");
            Console.ReadLine();
        }
    }
}
