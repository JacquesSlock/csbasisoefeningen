﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace somVoorgaandeGetallen
{
    class Program
    {
        static void Main(string[] args)
        {
            //oefening 10 : maak de som van alle voorafgaande getallen
            int intGetal, intSom = 0;
            string strRij = "";

            //invoer
            Console.WriteLine("Geef een getal: ");
            intGetal = Convert.ToInt16(Console.ReadLine());

            //maak de som
            for (int intTeller = 1; intTeller <= intGetal; intTeller++)
            {
                intSom += intTeller;
                if (intTeller == intGetal)
                    strRij += intTeller;
                else
                    strRij += intTeller + "+";

            }

            //uitvoer
            Console.Write("De som is: " + intSom);
            Console.Write(" (" + strRij + ")");
            Console.ReadLine();
        }
    }
}
