﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace gemiddeldeTienGetallen
{
    class Program
    {
        static void Main(string[] args)
        {
            int intTotaal = 0;
            double dblGemiddelde;

            for (int i = 1; i <= 10; i++)
            {
                Console.WriteLine("Geef getal " + i + ": ");
                intTotaal += Convert.ToInt32(Console.ReadLine());
            }
            dblGemiddelde = intTotaal / 10.0;

            Console.WriteLine("Het gemiddelde is " + dblGemiddelde);
            Console.ReadLine();
        }
    }
}
