﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace aantalKeerLetterInZin
{
    class Program
    {
        static void Main(string[] args)
        {
            //oefening 11:tel een letter in een zin
            string strZin, strInvoerLetter, strLetter;
            int intTeller, intAantalKeer = 0;

            //invoer
            Console.Write("Geef een zin in: ");
            strZin = Console.ReadLine();
            Console.Write("Welke letter wil je tellen? ");
            strInvoerLetter = Console.ReadLine();

            //verwerking

            //elke letter van de zin overlopen 
            for (intTeller = 0; intTeller <= strZin.Length - 1; intTeller++)
            {
                strLetter = strZin.Substring(intTeller, 1);

                if (strLetter == strInvoerLetter)
                {
                    //bijhouden hoeveel keer die letter voorkomt:
                    intAantalKeer += 1;//    intAantalKeer++;
                }
            }

            //uitvoer
            Console.WriteLine("De letter " + strInvoerLetter + " komt " + intAantalKeer + " voor in de zin.");
            Console.ReadLine();
        }
    }
}
