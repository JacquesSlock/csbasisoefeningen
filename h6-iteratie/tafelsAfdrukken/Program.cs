﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tafelsAfdrukken
{
    class Program
    {
        static void Main(string[] args)
        {
            int intTafel, intAantal, intUitkomst;

            Console.Write("Tafel van: ");
            intTafel = Convert.ToInt32(Console.ReadLine());

            Console.Write("Aantal: ");
            intAantal = Convert.ToInt32(Console.ReadLine());

            for (int i = 1; i <= intAantal; i++)
            {
                intUitkomst = i * intTafel;
                Console.WriteLine(i + " x " + intTafel + " = " + intUitkomst);
            }

            Console.ReadLine();
        }
    }
}
