﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tienMachtenVanTwee
{
    class Program
    {
        static void Main(string[] args)
        {
            //oefening 6-2: eerste 10 machten van 2
            int intTeller;

            //verwerking en uitvoer
            for (intTeller = 0; intTeller < 10; intTeller++)
            {
                Console.WriteLine(Math.Pow(2, intTeller));
            }

            Console.ReadKey();
        }
    }
}
