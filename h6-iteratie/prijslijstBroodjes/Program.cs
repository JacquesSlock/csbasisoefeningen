﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prijslijstBroodjes
{
    class Program
    {
        static void Main(string[] args)
        {
            //variabelen declareren
            int intTeller;
            double dblPrijs;

            //invoer
            Console.Write("Prijs van 1 broodje: ");
            dblPrijs = Convert.ToDouble(Console.ReadLine());

            //toon de lijst
            for (intTeller = 1; intTeller <= 15; intTeller++)
            {
                if (intTeller == 1)
                {
                    Console.WriteLine("1 broodje: " + dblPrijs.ToString("0.00") + " EUR");
                }
                else
                {
                    Console.WriteLine(intTeller + " broodjes: "
                                     + (dblPrijs * intTeller).ToString("0.00") + " EUR");
                }
            }

            Console.ReadLine();
        }
    }
}
