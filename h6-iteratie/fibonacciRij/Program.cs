﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace fibonacciRij
{
    class Program
    {
        static void Main(string[] args)
        {
            //oefening 6-9
            //DE RIJ VAN FIBONACCI: 0 1 1 2 3 5 8 13 21 ...
            int intGetal1, intGetal2, intSom, intTeller;

            //gegeven
            intGetal1 = 0;
            intGetal2 = 1;

            //toon de eerste 2 getallen:
            Console.Write("0 1 ");

            //bereken en toon de volgende 10 getallen:
            for (intTeller = 1; intTeller <= 10000; intTeller++)
            {
                //bereken de som
                intSom = intGetal1 + intGetal2;

                //toon de som
                Console.Write(intSom + " ");

                intGetal1 = intGetal2;
                intGetal2 = intSom;
            }

            Console.ReadLine();
        }
    }
}
