﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mosselFestijn
{
    class Program
    {
        static void Main(string[] args)
        {
            //Declareren
            double dblHFriet, dblHKNGHapje, dblHIjs, dblHDrank, dblPFriet, dblPKNGHapje, dblPIjs, dblPDrank, dblTOT;
            double cdblPFriet = 20, cdblPKNGHapje = 10, cdblPIjs = 3, cdblPDrank = 2;

            //Invoer
            Console.Write("Hoeveel mosselen met Friet: ");
            dblHFriet = Convert.ToInt16(Console.ReadLine());
            Console.Write("Hoeveel koninginenhapjes: ");
            dblHKNGHapje = Convert.ToInt16(Console.ReadLine());
            Console.Write("Hoeveel kinderijsjes: ");
            dblHIjs = Convert.ToInt16(Console.ReadLine());
            Console.Write("Hoeveel drankjes: ");
            dblHDrank = Convert.ToInt16(Console.ReadLine());

            //Verwerking
            dblPFriet = dblHFriet * cdblPFriet;
            dblPKNGHapje = dblHKNGHapje * cdblPKNGHapje;
            dblPIjs = dblHIjs * cdblPIjs;
            dblPDrank = dblHDrank * cdblPDrank;
            dblTOT = dblPFriet + dblPKNGHapje + dblPIjs + dblPDrank;

            //Uitvoer
            Console.WriteLine("Het totaal te betalen bedrag is " + dblTOT + " EUR.");
            Console.ReadKey();
        }
    }
}
