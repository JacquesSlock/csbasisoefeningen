﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vrachtwagenVerhuur
{
    class Program
    {
        static void Main(string[] args)
        {
            //Declareren
            double dblDPrijs, dblHuurDagen, dblPDagen, dblKMVertrek, dblKMAankomst, dblPKM, dblTOTHuurprijs, dblTOT;
            double cdblPKM = 0.20;

            //Invoer
            Console.Write("dagprijs: ");
            dblDPrijs = Convert.ToInt16(Console.ReadLine());
            Console.Write("Aantal gehuurde dagen: ");
            dblHuurDagen = Convert.ToInt16(Console.ReadLine());
            Console.Write("Kilometers bij vertrek: ");
            dblKMVertrek = Convert.ToInt16(Console.ReadLine());
            Console.Write("Kilometers bij aankomst: ");
            dblKMAankomst = Convert.ToInt16(Console.ReadLine());

            //Verwerking
            dblPKM = (dblKMAankomst - dblKMVertrek) * cdblPKM;
            dblPDagen = dblDPrijs * dblHuurDagen;
            dblTOT = dblPKM + dblPDagen;

            //Uivoer
            Console.WriteLine("De totale huurprijs bedraagt: €" + dblTOT + ".");
            Console.ReadKey();
        }
    }
}
