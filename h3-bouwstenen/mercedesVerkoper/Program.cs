﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mercedesVerkoper
{
    class Program
    {
        static void Main(string[] args)
        {
            //Declareren
            double dblPbmw, dblExclBTW;
            double cdblBTW = 0.79;

            //Invoer
            Console.Write("Prijs van de BMW: ");
            dblPbmw = Convert.ToInt16(Console.ReadLine());

            //Verwerking
            dblExclBTW = dblPbmw * cdblBTW;

            //Uitvoer
            Console.WriteLine("De prijs exclusief BTW bedraagt: " + dblExclBTW + " EUR.");
            Console.ReadKey();
        }
    }
}
