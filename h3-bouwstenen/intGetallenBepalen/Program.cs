﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace intGetallenBepalen
{
    class Program
    {
        static void Main(string[] args)
        {
            int intGetal1 = 1;
            int intGetal2 = 2;
            int intGetal3 = 3;
            int intGetal4 = 4;
            int intGetal5 = 5;
            intGetal2 = intGetal5 * 2;
            intGetal3 = intGetal2 / 2;
            intGetal4 += intGetal2;
            intGetal2 = intGetal4 + 2;
            intGetal5 = intGetal3 % 2;
            Console.WriteLine(
                "Waarde voor intGetal1: " + intGetal1 +
                " Waarde voor intGetal2: " + intGetal2 +
                 " Waarde voor intGetal3: " + intGetal3 +
                  " Waarde voor intGetal4: " + intGetal4 +
                 " Waarde voor intGetal5: " + intGetal5);

            Console.ReadLine();

        }
    }
}
