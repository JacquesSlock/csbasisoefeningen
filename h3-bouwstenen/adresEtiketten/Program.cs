﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace adresEtiketten
{
    class Program
    {
        static void Main(string[] args)
        {
            //declaratie
            string strVoornaam, strAchternaam, strStraat, strGemeente, strUitvoer;
            int intHuisnummer, intPostcode;


            //invoer
            Console.Write("Geef je voornaam: ");
            strVoornaam = Console.ReadLine();

            Console.Write("Geef je achternaam: ");
            strAchternaam = Console.ReadLine();

            Console.Write("Geef je straatnaam: ");
            strStraat = Console.ReadLine();

            Console.Write("Geef je huisnummer: ");
            intHuisnummer = Convert.ToInt32(Console.ReadLine());

            Console.Write("Geef je postcode: ");
            intPostcode = Convert.ToInt32(Console.ReadLine());

            Console.Write("Geef je gemeente: ");
            strGemeente = Console.ReadLine();

            //verwerking
            strUitvoer = "-----" + strVoornaam + " " + strAchternaam + "-----";
            strUitvoer += "-----" + strStraat + " " + intHuisnummer + "-----";
            strUitvoer += "-----" + intPostcode + " " + strGemeente + "-----";


            //uitvoer
            Console.WriteLine(strUitvoer);
            Console.ReadLine();
        }
    }
}
