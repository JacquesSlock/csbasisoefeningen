﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace maandweddeVertegenwoordigerBerekenen
{
    class Program
    {
        static void Main(string[] args)
        {
            //declaratie
            double dblMaandwedde, dblOmzet;
            const double cdblVasteWedde = 958.36;
            const double cdblPercentage = 0.0825;

            //invoer
            Console.Write("Omzet: ");
            dblOmzet = Convert.ToDouble(Console.ReadLine());

            //verwerking
            dblMaandwedde = dblOmzet * cdblPercentage + cdblVasteWedde;

            //uitvoer
            Console.Write("Maandwedde: ");
            Console.WriteLine(dblMaandwedde);
            Console.ReadLine();
        }
    }
}
