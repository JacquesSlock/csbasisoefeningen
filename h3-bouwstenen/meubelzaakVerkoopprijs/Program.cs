﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace meubelzaakVerkoopprijs
{
    class Program
    {
        static void Main(string[] args)
        {
            //Declareren
            Double dblPrijs, dblWinst, dblVKPrijs;
            double cdblMarge = 1.90;
            //Invoer
            Console.Write("Geef de prijs in: ");
            dblPrijs = Convert.ToInt16(Console.ReadLine());

            //Verwerking
            dblWinst = dblPrijs * cdblMarge;
            dblVKPrijs = dblWinst + dblPrijs;

            //Uitvoer
            Console.WriteLine("De Winst met winstmarge " + cdblMarge + " is " + dblWinst + " en de verkoopprijs is dan " + dblVKPrijs + ".");
            Console.ReadKey();
        }
    }
}
