﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace verkoopprijsBerekenenMetWinstmarge
{
    class Program
    {
        static void Main(string[] args)
        {
            //Declareren
            Double dblPrijs, dblWinst, dblVKPrijs;
            double cdblMarge = 0.35;
            //Invoer
            Console.Write("Geef de prijs in: ");
            dblPrijs = Convert.ToInt16(Console.ReadLine());

            //Verwerking
            dblWinst = dblPrijs * cdblMarge;
            dblVKPrijs = dblWinst + dblPrijs;

            //Uitvoer
            Console.WriteLine("De Winst met winstmarge " + cdblMarge + " is " + dblWinst + " en de verkoopprijs is dan " + dblVKPrijs + ".");
            Console.ReadKey();
        }
    }
}
