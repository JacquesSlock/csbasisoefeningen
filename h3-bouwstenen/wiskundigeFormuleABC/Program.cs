﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace wiskundigeFormuleABC
{
    class Program
    {
        static void Main(string[] args)
        {
            //declaratie
            double dblA, dblB, dblC, dblY;

            //invoer
            Console.Write("Geef getal A in: ");
            dblA = Convert.ToDouble(Console.ReadLine());
            Console.Write("Geef getal B in: ");
            dblB = Convert.ToDouble(Console.ReadLine());
            Console.Write("Geef getal C in: ");
            dblC = Convert.ToDouble(Console.ReadLine());

            //verwerking
            dblY = dblA * (dblB / dblC);

            //uitvoer
            Console.WriteLine("Het resultaat van " + dblA + " * (" + dblB + " / " + dblC + ") is gelijk aan " + dblY);
            Console.ReadLine();
        }
    }
}
