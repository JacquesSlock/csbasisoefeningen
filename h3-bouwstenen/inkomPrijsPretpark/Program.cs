﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace inkomPrijsPretpark
{
    class Program
    {
        static void Main(string[] args)
        {
            //declareren
            double dblK, dblV, dblPK, dblPV, dblTOT;
            double cdblPK = 10, cdblPV = 25;

            //Invoer
            Console.Write("Hoeveel personen t.e.m. 12 jaar zijn er: ");
            dblK = Convert.ToInt16(Console.ReadLine());
            Console.Write("Hoeveel personen boven de 12 jaar zijn er: ");
            dblV = Convert.ToInt16(Console.ReadLine());

            //Verwerking
            dblPK = dblK * cdblPK;
            dblPV = dblV * cdblPV;
            dblTOT = dblPK + dblPV;

            //Uitvoer
            Console.WriteLine("U moet " + dblTOT + " EUR betalen.");
            Console.ReadKey();
        }
    }
}
