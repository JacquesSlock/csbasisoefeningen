﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace celciusNaarFahrenheitOmzetten
{
    class Program
    {
        static void Main(string[] args)
        {
            //Declareren
            double dblCelsius, dblFahrenheit;

            //Invoer
            Console.Write("Geef een graden in Celsius in: ");
            dblCelsius = Convert.ToInt16(Console.ReadLine());

            //Verwerking
            dblFahrenheit = 1.8 * dblCelsius + 32;

            //Uitvoer
            Console.WriteLine("Een temperatuur van " + dblCelsius + " graden Celsius komt overeen met " + dblFahrenheit + " graden Fahrenheit.");
            Console.ReadKey();
        }
    }
}
