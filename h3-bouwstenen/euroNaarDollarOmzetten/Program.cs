﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace euroNaarDollarOmzetten
{
    class Program
    {
        static void Main(string[] args)
        {
            //Declareren
            double dblEuro, dblDollar;
            double cdblKoers = 1.2231;

            //Invoer
            Console.Write("Geef het bedrag in euro in: ");
            dblEuro = Convert.ToInt16(Console.ReadLine());

            //Verwerking
            dblDollar = dblEuro * cdblKoers;

            //Uitvoer
            Console.WriteLine(dblEuro + " EUR is gelijk aan " + dblDollar + " USD.");
            Console.ReadKey();
        }
    }
}
