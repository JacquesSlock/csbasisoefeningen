﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace stipwinkelFactuurbedrag
{
    class Program
    {
        static void Main(string[] args)
        {
            //declaratie 
            string strNaam;
            int intAantalStrips;
            const double cdblBTW = 0.06;
            const int cintPrijs = 5;
            double dblSubtotaal, dblBTWBedrag, dblTotaalprijs;

            //invoer
            Console.Write("Wat is je naam?: ");
            strNaam = Console.ReadLine();

            Console.Write("Geef het aantal strips in: ");
            intAantalStrips = Convert.ToInt32(Console.ReadLine());

            //verwerking
            dblSubtotaal = intAantalStrips * cintPrijs;
            dblBTWBedrag = cdblBTW * dblSubtotaal;
            dblTotaalprijs = dblSubtotaal + dblBTWBedrag;

            //uitvoer
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("#######################################");
            Console.WriteLine();

            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("U wordt geholpen door " + strNaam);
            Console.WriteLine();
            Console.WriteLine("Eenheidsprijs exclusief BTW = " + cintPrijs + " EUR");
            Console.WriteLine("Aantal = " + intAantalStrips);
            Console.WriteLine();

            Console.WriteLine("Subtotaal exclusief BTW = " + dblSubtotaal + " EUR");
            Console.WriteLine();
            Console.WriteLine("BTW-tarief = " + cdblBTW * 100 + "%");
            Console.WriteLine("BTW bedrag = " + dblBTWBedrag + " EUR");

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Totaat inclusief BTW = " + dblTotaalprijs + " EUR");

            Console.ReadLine();
        }
    }
}
